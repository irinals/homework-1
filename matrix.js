var matrix = [
[3,6,9,12],
[2,5,8,11],
[1,4,7,10]
];

function transpose(){
matrix=matrix.reverse();
var new_matrix = [];
for (var i=0; i<matrix.length; i++){
	for (var j=0; j<matrix[i].length; j++){
		if (matrix[i][j] === undefined) continue;
		if (new_matrix[j] === undefined) new_matrix[j] = [];
		new_matrix[j][i] = matrix[i][j];
	}
}
return new_matrix;
}
console.log(transpose(matrix));

